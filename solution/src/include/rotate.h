#ifndef ROTATE_HEADER
#define ROTATE_HEADER

#include "image.h"

struct image rotate(struct image *source);

#endif
