#ifndef IMAGE_HEADER
#define IMAGE_HEADER

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

struct __attribute__((packed)) pixel {
  uint8_t r, g, b;
};

struct image {
  uint64_t width, height;
  struct pixel *data;
};

/*  deserializer   */
enum read_status {
  READ_OK = 0,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS,
  READ_INVALID_HEADER,
  READ_INVALID_CONTENT
};

unsigned long get_pixel_index(const struct image *image, uint64_t row,
                              uint64_t column);

void set_pixel(struct image *image, const struct pixel pixel, uint64_t row,
               uint64_t column);

struct pixel get_pixel(const struct image *image, uint64_t row,
                       uint64_t column);
void delete_image(struct image *image);
struct image create_image(uint64_t width, uint64_t height);

#endif
