#ifndef BMP_HEADER
#define BMP_HEADER

#include "../image.h"

#define WRITE_STATUSES 3
#define READ_STATUSES 5

enum read_status from_bmp(FILE *in, struct image *image);
/*  serializer   */
enum write_status { WRITE_OK = 0, WRITE_INVALID_HEADER, WRITE_INVALID_CONTENT };

enum write_status to_bmp(FILE *out, struct image *image);
uint32_t bmp_image_size(const struct image *image);

#endif
