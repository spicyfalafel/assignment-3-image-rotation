#include "../include/formats/bmp.h"
#include "../include/rotate.h"
#include "../include/util.h"
#include <stdlib.h>

static char *write_status_messages[WRITE_STATUSES] = {
    [WRITE_INVALID_HEADER] = "Invalid header",
    [WRITE_INVALID_CONTENT] = "Invalid file content"};

static char *read_status_messages[READ_STATUSES] = {
    [READ_INVALID_SIGNATURE] = "Invalid signature",
    [READ_INVALID_BITS] = "Invalid bits",
    [READ_INVALID_HEADER] = "Invalid header",
    [READ_INVALID_CONTENT] = "Invalid file content"};

void rotate_bmp(FILE *in, FILE *out, struct image *source_image,
                struct image *result_image) {
  enum read_status read_status = from_bmp(in, source_image);
  if (read_status != READ_OK) {
    fprintf(stderr, "%s", read_status_messages[read_status]);
    delete_image(source_image);
    exit(1);
  }

  *result_image = rotate(source_image);
  if (result_image != NULL && result_image->data != NULL) {
    enum write_status write_status = to_bmp(out, result_image);
    if (write_status != WRITE_OK) {
      fprintf(stderr, "%s", write_status_messages[write_status]);
      delete_image(result_image);
      delete_image(source_image);
      exit(1);
    }
  } else {
      delete_image(result_image);
      delete_image(source_image);
      exit(1);
  }
}

int main(int args, char **argv) {
  if (args < 3) {
    fprintf(stderr, "Args: <source-image> <transformed-image>");
    return 1;
  }
  FILE *in = NULL;
  FILE *out = NULL;
  char *inp_name = argv[1];
  char *out_name = argv[2];
  struct image source_image = {0};
  struct image result_image = {0};

  in = fopen(inp_name, "rb");
  check_file(in, inp_name);
  out = fopen(out_name, "wb");
  check_file(out, out_name);
  rotate_bmp(in, out, &source_image, &result_image);
  if (fclose(in) != 0 || fclose(out) != 0) {
    fprintf(stderr, "Error on closing files");
    delete_image(&result_image);
    delete_image(&source_image);
    exit(1);
  }

  delete_image(&source_image);
  delete_image(&result_image);
  exit(0);
}
