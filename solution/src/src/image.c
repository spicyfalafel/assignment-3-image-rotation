#include "../include/image.h"

unsigned long get_pixel_index(const struct image *image, uint64_t row, uint64_t column) {
  return row * image->width + column;
}

void set_pixel(struct image *image, const struct pixel pixel, uint64_t row,
               uint64_t column) {
  image->data[get_pixel_index(image, row, column)] = pixel;
}

struct pixel get_pixel(const struct image *image, uint64_t row,
                       uint64_t column) {
  return image->data[get_pixel_index(image, row, column)];
}

struct image create_image(uint64_t width, uint64_t height) {
  struct image res = {.width = width, .height=height};
  return res;
}

void delete_image(struct image *image) { 
	free(image->data);
	image->data = NULL;
}
