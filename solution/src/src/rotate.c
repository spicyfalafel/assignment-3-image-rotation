#include "../include/rotate.h"
#include <malloc.h>

/* создаёт копию изображения, которая повёрнута на 90 градусов */
struct image rotate(struct image *source_image) {
  uint64_t h = source_image->height;
  uint64_t w = source_image->width;
  struct image result = create_image(h, w);
  result.data = malloc(sizeof(struct pixel) * h * w);
  if (result.data == NULL) {
	  return result;
  }
  for (size_t i = 0; i < w; i++) {
    for (size_t j = 0; j < h; j++) {
      struct pixel pixel = get_pixel(source_image, j, i);
      set_pixel(&result, pixel, i, (result.width - 1 - j));
    }
  }
  return result;
}
